import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components/App';
import { BrowserRouter } from 'react-router-dom';
import { makeStore } from './core/store';
import { Provider } from 'react-redux';
import 'bootstrap/scss/bootstrap.scss';
import './components/App.scss';

const store = makeStore(window.__INITIAL_STATE__);

const getElement = () => {
    let element = document.getElementById('root');

    return element;
};

const render = () => ReactDOM.render(
    <Provider store={ store }>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    getElement()
);

render();
