import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'
import { LinkContainer } from 'react-router-bootstrap';
import { Navbar, Nav, NavItem, Grid } from 'react-bootstrap';
import { Album } from './Album';
import { Albums } from './Albums';
import { Search } from './Search';

export class AppComponent extends Component {
    render () {
        return (
            <div>
                <Navbar fixedTop={ true } expand="lg" className="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
                    <Nav>
                        <LinkContainer to={ '/albums' } className="nav-item">
                            <NavItem eventKey={ 1 } className="nav-link">Сохраненное</NavItem>
                        </LinkContainer>
                        <LinkContainer to={ '/search' } className="nav-item">
                            <NavItem eventKey={ 2 } className="nav-link">Поиск</NavItem>
                        </LinkContainer>
                    </Nav>
                </Navbar>
                <Grid componentClass={ 'main' }>
                    <Switch>
                        <Route path='/' exact={ true } component={ Albums } />
                        <Route path='/album/:id' component={ Album } />
                        <Route path='/albums' component={ Albums } />
                        <Route path='/search' component={ Search } />
                    </Switch>
                </Grid>
            </div>
        );
    }
}

export const App = AppComponent;
