import React, { Component } from 'react';
import { Row, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AlbumsCollectionComponent } from './Modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import emptyImg from '../img/empty.gif';

export class AlbumsComponent extends Component {
    render() {
        const { items } = this.props;
        return (
            <div>
                <AlbumsCollectionComponent items={ items } />
                <Row>
                    {
                        !items || items.size ? null :
                            <p className="empty-notice">
                                <Image src={emptyImg} alt="" className="empty-cover" />
                                <br />
                                Пусто. Добавить альбом можно на <Link to={ '/search' }>странице поиска</Link>.
                            </p>
                    }
                </Row>
            </div>
        );
    }
}

export const Albums = connect(
    (state, ownProps) => ({
        items: state.getIn(['albums', 'items'])
    }),
    (dispatch, ownProps) => bindActionCreators({

    }, dispatch)
)(AlbumsComponent);
