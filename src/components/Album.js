import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Grid, Row, Col,
    Alert, Button,
    Table
} from 'react-bootstrap';
import  { LoadingComponent, ErrorComponent, CoverComponent } from './Modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getAlbum, addAlbum, removeAlbum } from '../core/actions/albums';


class MediaComponent extends Component {
    static propTypes = {
        media: PropTypes.object
    };

    render() {
        const { media } = this.props;
        return !media ? null : (
            <Table>
                <thead>
                    <tr>
                        <th colSpan={ 2 }>{ media.get('title') || `Media #${media.get('position')}` }</th>
                    </tr>
                </thead>
                <tbody>
                {
                    media.get('tracks').map((track) =>
                        <tr key={ track.get('id') }>
                            <td>{ track.get('position') }.</td>
                            <td>{ track.get('title') }</td>
                        </tr>
                    )
                }
                </tbody>
            </Table>
        );
    }
}

export class AlbumComponent extends Component {
    componentDidMount() {
        const { id, getAlbum } = this.props;
        if (id)
            getAlbum(id);
    }

    componentWillReceiveProps(nextProps) {
        const { id, getAlbum } = nextProps;
        if (this.props.id !== id)
            getAlbum(id);
    }

    render() {
        const {
            loading, error, item, collected,
            addAlbum, removeAlbum
        } = this.props;
        return (
            <Grid fluid={ true }>
                {
                    !item || loading || error ? null :
                        <Row>
                            <Col md={ 4 }>
                                <CoverComponent image={ item.getIn(['cover', 'image']) } />
                            </Col>
                            <Col md={ 8 }>
                                <div className="album-info">
                                    <h1 className="title">
                                        { item.get('title') }
                                    </h1>
                                    <div className="summary">
                                        <div>
                                            <span>Исполнитель: </span>
                                            <span className="artist">
                                                {
                                                    item.get('artist-credit').map((credit) => credit.getIn(['artist', 'name'])).join(', ')
                                                }
                                            </span>
                                        </div>
                                        <div>
                                            {
                                                !item.has('date') ? null :
                                                    new Date(item.get('date')).getFullYear() }
                                        </div>
                                    </div>
                                </div>
                                <Alert bsStyle={ 'warning' }>
                                    {
                                        collected ?
                                            'Сохранено' :
                                            'Не сохранено'
                                    }
                                    { ': ' }
                                    <Button className="pull-right ml-2" bsSize={ 'small' } bsStyle={ collected ? 'danger': 'success'}
                                            onClick={ () => (collected ? removeAlbum : addAlbum)(item.get('id')) }
                                    >
                                        { collected ? 'Удалить' : 'Сохранить' }
                                    </Button>
                                </Alert>
                            </Col>
                        </Row>
                }
                {
                    !item || !item.has('media') ? null : item.get('media').map((media, key) =>
                        <Row key={ key }>
                            <Col md={ 8 } mdPush={ 4 }>
                                <MediaComponent media={ media } />
                            </Col>
                        </Row>
                    )
                }
                <Row>
                    <Col>
                        <LoadingComponent loading={ loading }>Загрузка...</LoadingComponent>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ErrorComponent error={ error }>Ошибка:</ErrorComponent>
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export const Album = connect(
    (state, ownProps) => {
        const albums = state.getIn(['albums', 'items']);
        const id = ownProps.match.params.id;
        return {
            id,
            loading: state.getIn(['album', 'items', id, 'loading'], false),
            error: state.getIn(['album', 'items', id, 'error']),
            item: state.getIn(['album', 'items', id]),
            collected: albums && albums.find((album) => album.get('id') === id)
        };
    },
    (dispatch, ownProps) => bindActionCreators({
        getAlbum, addAlbum, removeAlbum
    }, dispatch)
)(AlbumComponent);
