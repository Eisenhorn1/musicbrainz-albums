import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'react-infinite-scroller';
import {
    Grid, Col, Row,
    FormGroup, InputGroup, FormControl, Button, Glyphicon
} from 'react-bootstrap';
import { LoadingComponent, ErrorComponent, InfoComponent, AlbumsCollectionComponent } from './Modules';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { searchAlbums, updateSearch } from '../core/actions/search';


class InputComponent extends Component {
    static propTypes = {
        value: PropTypes.string,
        delay: PropTypes.number,
        onQuery: PropTypes.func
    };

    static defaultProps = {
        value: '',
        delay: 0
    };

    state = {
        value: '',
        timer: undefined
    };

    componentDidMount() {
        const { value } = this.props;
        this.setState({ value });
    }

    componentWillReceiveProps(nextProps) {
        const { value } = nextProps;
        if (this.props.value !== value)
            this.setState({ value });
    }

    componentWillUnmount() {
        clearTimeout(this.state.timer);
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({ value });
        this.query(value, this.props.delay);
    }

    handleKeyPress(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            this.query(this.state.value)
        }
    }

    handleClick() {
        this.query(this.state.value);
    }

    query(value, delay) {
        const { onQuery } = this.props;
        clearTimeout(this.state.timer);
        if (onQuery)
            !delay ? onQuery(value) : this.setState({
                timer: setTimeout(() => onQuery(value), delay)
            });
    }

    render() {
        return (
            <form>
                <FormGroup>
                    <InputGroup>
                        <FormControl type="text" value={ this.state.value }
                                     onChange={ this.handleChange.bind(this) } onKeyPress={ this.handleKeyPress.bind(this) } />
                        <InputGroup.Button>
                            <Button onClick={ this.handleClick.bind(this) }><Glyphicon glyph="search" /></Button>
                        </InputGroup.Button>
                    </InputGroup>
                </FormGroup>
            </form>
        );
    }
}


export class SearchComponent extends Component {
    componentDidMount() {
        const { history, updateSearch } = this.props;
        const query = new URLSearchParams(history.location.search).get('query') || '';
        if (query)
            updateSearch(query);
        else {
            const { query } = this.props;
            if (query)
                this.handleQuery(query);
        }
    }

    componentWillReceiveProps(nextProps) {
        const { query } = nextProps;
        this.search(query);
    }

    handleQuery(query) {
        const { history, updateSearch } = this.props;
        history.replace({ ...history.location, search: new URLSearchParams({ query }).toString() });
        updateSearch(query);
    }

    handleMore() {
        const { query, items, searchAlbums } = this.props;
        if (items)
            searchAlbums(query, 8, items.size);
    }

    search(query) {
        const { searchAlbums } = this.props;
        if (this.props.query !== query) {
            searchAlbums(query, 16);
        }
    }

    render() {
        const {
            loading, error,
            query, more, items
        } = this.props;
        return (
            <Grid fluid={ true }>
                <Row>
                    <Col>
                        <InputComponent value={ query } delay={ 500 } onQuery={ this.handleQuery.bind(this) }/>
                    </Col>
                </Row>
                <InfiniteScroll element='div' initialLoad={ false }
                                hasMore={ more } loadMore={ this.handleMore.bind(this) }
                >
                    <AlbumsCollectionComponent items={ items } />
                </InfiniteScroll>
                <Row>
                    <Col>
                        <LoadingComponent loading={ loading }>Загрузка...</LoadingComponent>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <ErrorComponent error={ error }>Ошибка:</ErrorComponent>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <InfoComponent>
                            { !error && items && !items.size ? 'Ничего не найдено' : null }
                        </InfoComponent>
                    </Col>
                </Row>
            </Grid>
        );
    }
}



export const Search = withRouter(connect(
    (state, ownProps) => {
        return {
            loading: state.getIn(['search', 'loading'], false),
            error: state.getIn(['search', 'error']),
            query: state.getIn(['search', 'query'], ''),
            more: state.getIn(['search', 'more']),
            items: state.getIn(['search', 'items'])
        };
    },
    (dispatch, ownProps) => bindActionCreators({
        searchAlbums, updateSearch
    }, dispatch)
)(SearchComponent));
